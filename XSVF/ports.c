/********************************************************/
/* file: ports.c					*/
/********************************************************/

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

#include "ports.h"

// void Delay( uint32_t us );

#define XSVF_GPIO GPIOA
#define XSVF_RCC_APB2Periph_GPIO RCC_APB2Periph_GPIOA
#define TMS_Pin GPIO_Pin_4
#define TDI_Pin GPIO_Pin_5
#define TCK_Pin GPIO_Pin_6
#define TDO_Pin GPIO_Pin_7

void ports_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
        // Enable Peripheral Clocks
        RCC_APB2PeriphClockCmd ( XSVF_RCC_APB2Periph_GPIO, ENABLE );
        // Configure output pins
        GPIO_StructInit (&GPIO_InitStructure );
        GPIO_InitStructure.GPIO_Pin = TMS_Pin | TDI_Pin | TCK_Pin ;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
        GPIO_Init (XSVF_GPIO, &GPIO_InitStructure );
        // Configure input pin
        GPIO_StructInit (&GPIO_InitStructure );
        GPIO_InitStructure.GPIO_Pin = TDO_Pin;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
        GPIO_Init (XSVF_GPIO, &GPIO_InitStructure );
}

void set_port(uint8_t p,int val)
{
	static uint32_t BSRR_TMS, BSRR_TDI;

	if (p == TMS && val == 0) BSRR_TMS = (TMS_Pin << 16);
	if (p == TMS && val == 1) BSRR_TMS = (TMS_Pin);
	if (p == TDI && val == 0) BSRR_TDI = (TDI_Pin << 16);
	if (p == TDI && val == 1) BSRR_TDI = (TDI_Pin);

	/* clock TMS and TDI on falling TCK */
	if (p == TCK) {
		if (val == 0) {
			XSVF_GPIO->BSRR = BSRR_TMS | BSRR_TDI | (TCK_Pin << 16);
		} else {
			XSVF_GPIO->BSRR = TCK_Pin;
		}
	}
}

void pulse_clock()
{
	set_port(TCK,0);
	set_port(TCK,1);	/* clock period should be min 100 ns */
	set_port(TCK,0);
}

int read_tdo()
{
	return (XSVF_GPIO->IDR & TDO_Pin) ? 1 : 0 ;
}

/* Wait at least the specified number of microsec. */
void delay(long microsec)
{
//	_delay_ms(microsec>>12);
	set_port(TCK,0);
	while (--microsec > 0) {
		set_port(TCK,1);
		set_port(TCK,0);
	}
}

void hold(long microsec)
{
	while (--microsec > 0) {
		set_port(0,0);		/* nop */
	}
}
