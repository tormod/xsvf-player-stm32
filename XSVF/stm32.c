# include <stm32f10x.h>
# include <stm32f10x_usart.h>
#include "xprintf.h"

void myputchar ( unsigned char c)
{
        uart_putc (c, USART1 );
}

unsigned char mygetchar ()
{
        return uart_getc ( USART1 );
}

void system_init(void) {
	/* For xprintf() */
	xfunc_in = mygetchar;
	xfunc_out = myputchar;
        // Configure SysTick Timer for 1 ms ticks (delay functions)
        if ( SysTick_Config ( SystemCoreClock / 1000) )
                while (1);
}

// Timer code
__IO uint32_t TimingDelay ;

void Delay( uint32_t nTime ) {
        TimingDelay = nTime ;
        while ( TimingDelay != 0);
}

void SysTick_Handler (void) {
        if ( TimingDelay != 0x00)
        TimingDelay --;
}

#ifdef USE_FULL_ASSERT
void assert_failed ( uint8_t * file , uint32_t line)
{
        /* Infinite loop */
        /* Use GDB to find out why we're here */
        while (1);
}
#endif

