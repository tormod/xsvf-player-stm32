/********************************************************/
/* file: uart.c						*/
/********************************************************/

#define UART_BAUD 921600	// baudrate

#include <stm32f10x_usart.h>
#include "xsvf-uart.h"
#include "xsvf.h"
#include "uart.h"
#include "xprintf.h"

unsigned char buffer[0x100];
int buffer_pos;
int buffer_size;

void uart_putchar(char c);
char uart_getchar(void);

//FILE uart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);
//FILE *uart;

void uart_init(void)
{
	uart_open(UART, UART_BAUD, 0);
	uart_open(DEBUG_UART, UART_BAUD, 0);
	buffer_pos = 0;
	buffer_size = 0;
}

void uart_putchar(char c)
{
	uart_putc(c, UART);
}

char uart_getchar(void)
{
	return uart_getc( UART );
}

uint8_t read_next_instr()
{
	int n = 0;
	LOG_DEBUG("requesting next instruction");
	uart_putchar('+');
	while (n<=0) {
		n = load_next_instr(buffer);
	}
	LOG_DEBUG("received %d bytes",n);
	buffer_pos = 1;
	buffer_size = n;
	return buffer[0];
}

void fail(void)
{
	print_uart(UART,"f\n");
}

void success(void)
{
	print_uart(UART,"s\n");
}

int get_hex_value(char c)
{
	if (c>='0' && c<='9') {
		return c-'0';
	} else if (c>='a' && c<='f') {
		return c-'a'+10;
	} else if (c>='A' && c<='F') {
		return c-'A'+10;
	} else {
		return -1;
	}
}

int read_chunk(void)
{
	char c;
	unsigned char size;

	uart_putchar('+');

	c = uart_getchar();
	if (c!='+') {
		return 1;
	}

	size = uart_getchar()&0xff;

	buffer_size = 0;
	while (buffer_size<size) {
		buffer[buffer_size++] = uart_getchar();
	}
	LOG_DEBUG("read ok %02x",buffer_size);
	return 0;
}

int read_byte(uint8_t *data)
{
	if (buffer_pos>=buffer_size) {
		LOG_DEBUG("need more data");
		if (read_chunk()) {
			LOG_DEBUG("failed to get more data");
			return 1;
		}
		LOG_DEBUG("got more data");
		buffer_pos = 0;
	}
	*data = buffer[buffer_pos];
	buffer_pos++;
	return 0;
}

int read_word(uint16_t *data)
{
	uint8_t l,h;
	if (read_byte(&h)) {
		return 1;
	}
	if (read_byte(&l)) {
		return 1;
	}
	((uint8_t*)data)[0] = l;
	((uint8_t*)data)[1] = h;
	return 0;
}

int read_long(uint32_t *data)
{
	uint16_t l,h;
	if (read_word(&h)) {
		return 1;
	}
	if (read_word(&l)) {
		return 1;
	}
	((uint16_t*)data)[0] = l;
	((uint16_t*)data)[1] = h;
	return 0;
}

int read_bytes(uint8_t *data, int len)
{
	int i;
	for (i=len-1; i>=0; --i) {
		read_byte(&data[i]);
	}
}


