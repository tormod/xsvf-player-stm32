XSVF player for STM32
=====================

This is based on the Papilio code by Ben at
https://github.com/ben0109/XSVF-Player/

It uses the same client run on the computer to feed
the microcontroller with the decoded xsvf stream.
Get the client from above link.

Just make sure the baud rates are the same in the client
and in the microcontroller firmware!

The file structure in this repository is based on the
STM32-Template by Geoffrey Brown at
https://github.com/geoffreymbrown/STM32-Template

Copyright 2016 Tormod Volden

